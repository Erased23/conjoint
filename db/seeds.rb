# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


example_mouse = Product.create(name: "Super Gaming Mouuse", brand: "Genius")
example_monitor = Product.create(name: "Gaming Monitor HD", brand: "Samsung")

['DPI', 'Grip'].each do |em|
  example_mouse.characteristics.create(name: em)
end


['Inches', 'Color'].each do |emo|
  example_monitor.characteristics.create(name: emo)
end