class Level < ActiveRecord::Base
  belongs_to :characteristic
  validates :name,
          presence: true
end
