class Characteristic < ActiveRecord::Base
  belongs_to :product
  has_many   :levels, dependent: :destroy
  accepts_nested_attributes_for :levels,
                                reject_if: proc { |attributes| attributes['name'].blank? },
                                allow_destroy: true
  validates :name,
            presence: true
end
