class Product < ActiveRecord::Base
  has_many :characteristics, dependent: :destroy
  accepts_nested_attributes_for :characteristics,
                                reject_if: proc { |attributes| attributes['name'].blank? },
                                allow_destroy: true
  validates :name, :brand, 
            presence: true
end
